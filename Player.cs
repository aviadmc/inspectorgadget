﻿using UnityEngine;
using Infra;
using Infra.Gameplay;
using Infra.Utils;

namespace Gadget {

[RequireComponent(typeof(Animator), typeof(Rigidbody2D), typeof(OverlapChecker))]

/// Class of the main player
public class Player : MonoBehaviour {
    public int health = 1;
    public float jumpHeigh = 15f;
    public float speed = 7f;
    public float gravity = 2f;
    public float gunAngle = 45f;
    public float shotSpeed = 15f;
    
    // controllers:
    public KeyCode right;
    public KeyCode left;
    public KeyCode up;
    public KeyCode shoot;

    public Transform armTrans;
    public Transform handImgTrans;
    public Rigidbody2D bulletRB;

    private readonly int isAlive = Animator.StringToHash("Alive");
    private readonly int jumpTrig = Animator.StringToHash("Jump");

    private Animator anim;
    private Rigidbody2D rb;
    private OverlapChecker oc;

    private bool IsOverlapping {
        get {
            return oc.isOverlapping;
        }
    }

    // on start:
    protected void Awake() {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        oc = GetComponent<OverlapChecker>();

        anim.SetBool(isAlive, true);
    }

    protected void Update() {
        var armAngle = armTrans.eulerAngles;
        armAngle.z = gunAngle;
        armTrans.eulerAngles = armAngle;

        rb.gravityScale = gravity;

        var velocity = rb.velocity;
        // player action:
        if (Input.GetKeyDown(up) && IsOverlapping) {
            velocity.y = jumpHeigh;
            rb.velocity = velocity;
            anim.SetTrigger(jumpTrig);
        } else if (Input.GetKey(right)) {
            velocity.x = speed;
            rb.velocity = velocity;
        } else if (Input.GetKey(left)) {
            velocity.x = -speed;
            rb.velocity = velocity;
        } else if (Input.GetKey(shoot)) {
            if (!bulletRB.gameObject.activeInHierarchy) {
                bulletRB.gameObject.SetActive(true);
                bulletRB.position = handImgTrans.position;
                bulletRB.velocity = Vector2.right.Rotate(Mathf.Deg2Rad * gunAngle) * shotSpeed;
            }
        }
    }
        
    protected void OnCollisionEnter2D(Collision2D collision) {
        if (health <= 0) return; // player is dead

        if (collision.gameObject.CompareTag("Victory")) {
            // winning the game! horay!
            DebugUtils.Log("Great Job!");
            return;
        }
        if (!collision.gameObject.CompareTag("Enemy")) return;
        // from here - player hit an enemy

        --health;
        if (health > 0) return;
        // from here - player died
        anim.SetBool(isAlive, false);
        rb.velocity = Vector2.zero;
        rb.gravityScale = 4f;
        enabled = false;
    }
}
}
